package High;



import java.util.Date;

public abstract class  AbsMultiTimer implements ITimer {
        public abstract boolean abort(Date notifyDate, TimerClient client);// abort registered notification
        public abstract void abortAll(TimerClient client);// abort all registered notification of this client


        }
