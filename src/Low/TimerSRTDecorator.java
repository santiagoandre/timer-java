package Low;






import java.util.Date;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import High.AbsMultiTimer;
import High.TimerClient;

//Timer Shortest ramaining time
public class TimerSRTDecorator extends AbsMultiTimer implements TimerClient{
    private TreeSet<TimerReg> clients;
    private ImpSimpleTimer simpleTimer;
    public TimerSRTDecorator(){
        clients = new TreeSet<TimerReg>();
        simpleTimer = new ImpSimpleTimer();
    }
    public void setAccuracy(TimeUnit timeUnit, int quantity) {
		if(simpleTimer == null)
	        simpleTimer = new ImpSimpleTimer();
		simpleTimer.setAccuracy(timeUnit, quantity);
			
	}
    public TimerReg firstReg() {//retorna el registro  mas proximo a notificar
        if(clients.isEmpty())
            return null;
        return clients.first();
    }
    public boolean schedule(Date notifyDate, TimerClient client) {
        TimerReg newreg = new TimerReg(notifyDate,client);
        if (clients.contains(newreg)) {//ya esta programado ese cliente en esa fecha
            return false;
        }

        TimerReg firstReg = firstReg();
    	clients.add(newreg);        	
        if (clients.size() == 1){//es el unico regstro
            if(!simpleTimer.schedule(notifyDate, this)){
            	clients.pollFirst();
            	return false;
            }
            return true;
        }
        if(firstReg.getDate().compareTo(notifyDate)!=-1) //minusDate > notifyDate
            newFirst();

        return true;
    }


    @Override
    public boolean abort(Date notifyDate, TimerClient client) {
        if(clients.isEmpty())
            return false;//no existen tareas programadas
        TimerReg reg = new TimerReg(notifyDate,client);
        if(reg.equals(firstReg())) {
        	simpleTimer.abort();
            clients.pollFirst();
            next();
            return true;            
        }else {
        	return clients.remove(reg);
        }
    }

    @Override
	public  void abortAll(TimerClient client) {
    	if(clients.isEmpty())
    		return;
        TreeSet<TimerReg> clients = new TreeSet<TimerReg>();
        for(TimerReg reg: this.clients) {
            if (!reg.getClient().equals(client))
                clients.add(reg);
        }
        if(!clients.isEmpty() ){
        	if(firstReg().getClient().equals(client)){
	        	this.clients = clients;
	        	newFirst();//informa que hay un nuevo primer elemento
        	}
        }else{
        	simpleTimer.abort();
        }
        	//problema de seccion critica
    }



    private void newFirst() {
    	simpleTimer.setNotifyDate(firstReg().getDate());
	}

    public void timeOut() {
        TimerReg reg = clients.pollFirst();
        if(reg != null) {
            reg.getClient().timeOut();
        }
        next();
       
    }
	private void next() {
		if(!clients.isEmpty())
			simpleTimer.schedule(firstReg().getDate(),this);
	    
	}
	

}