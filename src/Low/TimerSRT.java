package Low;






import java.util.Date;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import High.AbsMultiTimer;
import High.TimerClient;

//Timer Shortest ramaining time
public class TimerSRT extends AbsMultiTimer {
    public static  int STOP = 0;
    public static  int RUNNING = 1;
    public static  int END = 2;
    
    private int state;
    
    private TreeSet<TimerReg> clients;
    
    private long firstTimeMillis;
    
    private Thread thread;
    
  //----------------------
  	//estos atributos reprecentan la precicion del timer
  	private TimeUnit timeUnit;
  	private long quantity;
  	//Ej: timeUnit es minutos, y quantity 10,
  	//cada 10 minutos  el timer va a evaluar si ya es
  	//notifyDate para notifcar

  	//Ej: timeUnit es dias, y quantity 1,
  	//cada 1 dias   el timer va a evaluar si ya es
  	//notifyDate para notifcar
  	//----------------------
    public TimerSRT(){
        clients = new TreeSet<TimerReg>();
    }
    //inicializar el hilo de la espera activa
    private void initThread() {
        thread = new Thread(){
        	public void run(){
        		runn();
        	}
        };
    }
   public TreeSet<TimerReg> getClients(){
	   return clients;
   }
   //programar una notificacion a un timerClient en una fecha especifica
    //	@Override
    public boolean schedule(Date notifyDate, TimerClient client) {

        TimerReg newreg = new TimerReg(notifyDate,client);
        if (clients.contains(newreg)) {//ya esta programado ese cliente en esa fecha
            return false;
        }
        if(! notifyDate.after(new Date())){// la fecha no es futura, notificacion de una//TODO 
        	client.timeOut();
        	return true;
        }
    	clients.add(newreg);        	
        if (clients.size() == 1){//es el unico regstro
        	firstTimeMillis = notifyDate.getTime();
        	initThread();
        	thread.start();
        }else if(firstTimeMillis > notifyDate.getTime()){ //minusDate > notifyDate
        	//System.out.println(new Date(firstTimeMillis) + "  > " +  notifyDate);
           firstTimeMillis  = notifyDate.getTime();
        }

        return true;
    }

    //abortar una notificacion programada
    //@Override
    public boolean abort(Date notifyDate, TimerClient client) {
        if(clients.isEmpty())
            return false;//no existen tareas programadas
        TimerReg reg = new TimerReg(notifyDate,client);
        boolean result  =  clients.remove(reg);
        if(reg.equals(clients.first())) {
        	firstTimeMillis = clients.first().getDate().getTime();   
        }
        return result;
    }

    //abortar todas notificaciones programadas para un timerClient
   // @Override
    public void abortAll(TimerClient client) {
    	if(clients.isEmpty())
    		return;
        TreeSet<TimerReg> clients = new TreeSet<TimerReg>();
        for(TimerReg reg: this.clients) {
            if (!reg.getClient().equals(client))
                clients.add(reg);
        }
        if(clients.isEmpty() ){
        	state = STOP;
        	firstTimeMillis = -1;
        }else if(this.clients.first().getClient().equals(clients)){
        	//se elimino el primer registro
        	firstTimeMillis = clients.first().getDate().getTime();
        }
    	this.clients = clients;
        	//problema de seccion critica
    }



    //ya se cumplio el tiempo mas corto.
    public void timeOut() {
        TimerReg reg = clients.pollFirst();
        if(reg != null) {
            reg.getClient().timeOut();
        }
        next();
       
    }
	//hay que continuar notificando al siguiente
    private void next() {

		if(!clients.isEmpty()){
			firstTimeMillis = clients.first().getDate().getTime();
			state = RUNNING;
		}else
			firstTimeMillis = -1;
	}

    //cambiar la presicion del timer
    // @Override
   	public void setAccuracy(TimeUnit timeUnit, int quantity) {
   		this.timeUnit = timeUnit;
   		this.quantity = quantity;		
   	}
    //la configuracion es presicion maxima?
   	private boolean withMaxAcurrancy() {
		return timeUnit == null;
	}
	//esperar un tiempo x, para continuar preguntando
   	private void ssleep() {
		try{timeUnit.sleep(quantity);}
		catch(Exception e){};
		
	}

   	//a continuacion metodos de la espera activa de la fecha mas proxima para notificar
   	//espera activa porque pueden cancelar la espera, o cambiar la fecha 
   	
   	//principal, este decide cual metodo run ejecutar
   	private void runn(){

        state = RUNNING;
        while(state == RUNNING){
			if(withMaxAcurrancy())
				runMaxAcurrancy();
			else
				runWithVariableAcurrancy();
        }
	}
   	//espera activa sin pausas
   	private void runMaxAcurrancy(){
		//notifica con la mayor presicion posible
        long currenttime = System.currentTimeMillis();
        //espera activa por que, que mas!!
        boolean lack;
        while (state == RUNNING){
            lack =  currenttime < firstTimeMillis;
            if(!lack){
                state = END;
                break;
            }
            currenttime = System.currentTimeMillis();
            
        }
        if(state == END){
          timeOut();
        }
	}
   	//espera activa con pausas
	private void runWithVariableAcurrancy(){
		//notifica con menor presicion
        long currenttime;
        //espera activa por que, que mas!!
        boolean lack;
        while (state == RUNNING ){
            currenttime = System.currentTimeMillis();
            lack =  currenttime < firstTimeMillis;
            if(!lack){
                state = END;
                break;
            }
            ssleep();//se espera para volver a preguntar
            
        }
        if(state == END){
          timeOut();
        }

	}
	
	
	
	


	
	

}