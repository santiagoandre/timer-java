package Low;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import High.AbsSimpleTimer;
import High.TimerClient;

public class ImpSimpleTimer extends AbsSimpleTimer {

    public static  int STOP = 0;
    public static  int RUNNING = 1;
    public static  int END = 2;

	
    
    private TimerReg reg;
    private Thread thread;
    private   int state;
    
    
    //----------------------
  	//estos atributos reprecentan la precicion del timer
  	private TimeUnit timeUnit;
  	private long quantity;
  	//Ej: timeUnit es minutos, y quantity 10,
  	//cada 10 minutos  el timer va a evaluar si ya es
  	//notifyDate para notifcar

  	//Ej: timeUnit es dias, y quantity 1,
  	//cada 1 dias   el timer va a evaluar si ya es
  	//notifyDate para notifcar
  	//----------------------

    public ImpSimpleTimer() {
        this.state = STOP;
    }
    


   
	
   // @Override
    public boolean schedule(Date notifyDate, TimerClient client) {
       if(reg != null) {
            return false;
        }
       //se compara la fecha con la fecha actual
       int compare = notifyDate.compareTo(new Date());

       reg = new TimerReg(notifyDate,client);
       if(compare != 1 ){//es la fecha de este mismo instante o pasada, is current date
    	   System.out.println("de una");
    	   timeOut();
       }else{
	       	initThread();
	        thread.start();
       }
       return true;
    }
   

    @Override
	public  boolean abort() {
        boolean result = state == RUNNING;
        state = STOP;
        reg = null;
        return result;
    }
    // @Override
   	public void setAccuracy(TimeUnit timeUnit, int quantity) {
   		this.timeUnit = timeUnit;
   		this.quantity = quantity;		
   	}
    private boolean withMaxPresicion() {
		return timeUnit == null;
	}
   	private  void ssleep() {
   		try {this.timeUnit.sleep(quantity);} 
   		catch (Exception e) {}
   	}
   	
    public TimerReg getReg(){
        return reg;
    }
    private Date getNotifyDate() {
        TimerReg t = getReg();
        if(t == null){
            return null;
        }
        return t.getDate();
    }
    public  void setNotifyDate(Date notifyDate){
    	TimerReg t = getReg();
    	if(t != null)
    		t.setDate(notifyDate);
    }



    private void timeOut() {
    	TimerClient t =getReg().getClient();
    	this.reg =null;
    	t.timeOut();
        
    }

    private void initThread() {
    	
        thread = new Thread(){
        	public void run(){
        		if(withMaxPresicion())
        			runWithMaxAcurrancy();
        		else
        			runWithVariableAcurrancy();
        	}
           
			public  void runWithMaxAcurrancy(){
            	//notifica con la mayor presicion posible
                long currenttime = System.currentTimeMillis();
                long endtime = getNotifyDate().getTime();
                //espera activa por que, que mas!!
                boolean lack = currenttime  < endtime;
                state = RUNNING;
                while (state == RUNNING && getNotifyDate() != null){
                    lack =  currenttime < endtime;
                    if(!lack){
                        state = END;
                        break;
                    }
                    currenttime = System.currentTimeMillis();
                    
                }
                if(state == END){
                  timeOut();
                }

            }
            public  void runWithVariableAcurrancy(){
            	//notifica con menor presicion
                long currenttime;
                long endtime = getNotifyDate().getTime();
                //espera activa por que, que mas!!
                boolean lack;
                state = RUNNING;
                while (state == RUNNING && getNotifyDate() != null){
                    currenttime = System.currentTimeMillis();
                    lack =  currenttime < endtime;
                    if(!lack){
                        state = END;
                        break;
                    }
                    ssleep();//se espera para volver a preguntar
                    
                }
                if(state == END){
                  timeOut();
                }

            }
        };
    }






}
