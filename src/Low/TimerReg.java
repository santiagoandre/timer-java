package Low;



import java.util.Date;

import High.TimerClient;

public class TimerReg implements  Comparable{
    private Date notifyDate;
    private TimerClient client;

    public TimerReg(Date notifyDate,TimerClient client) {
        this.notifyDate = notifyDate;
        this.client =client;
    }

    public Date getDate() {
        return notifyDate;
    }

    public void setDate(Date date) {
        this.notifyDate = date;
    }
    public TimerClient getClient() {
        return client;
    }
  //  @Override
    public int compareTo(Object o) {
        TimerReg other = (TimerReg)o;
        return this.notifyDate.compareTo(other.getDate());

    }
    public String toString(){
    	return client.toString();
    }
   


}
