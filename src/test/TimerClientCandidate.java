package test;

import java.util.Date;

import High.TimerClient;


public class TimerClientCandidate implements TimerClient {
	private String name;
	private Date date;
	public TimerClientCandidate(String name){
		this.name = name;
		Integer d = 9;
	}
	public void timeOut() {
		System.out.println(name + " notificado a las " + new Date());
		if(!equalsD()){
			System.out.println(name + " debia ser notificado a las " + date);
			
		}
		System.out.println("------------------------------------------");
	}
	private long restar(){
		return date.getTime()-(new Date()).getTime();
	}
	private boolean equalsD(){
		return (new Date()).getSeconds() ==date.getSeconds();
	}
	public String toString(){
		
		return name;
	}

	public void setDate(Date date){
		this.date = date;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimerClientCandidate other = (TimerClientCandidate) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}
