package test;

import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import Low.ImpSimpleTimer;
import Low.TimerSRT;


public class testSimpleTimer {
	private ImpSimpleTimer timer;
	private TimerSRT multitimer;
	Scanner reader = new Scanner(System.in);
	public void init_Multiple() throws InterruptedException{
		multitimer = new TimerSRT();
		multitimer.setAccuracy(TimeUnit.SECONDS, 4);
		System.out.print("ingrese numero de clientes: ");
		initClientes(0,reader.nextLong());
	}
	public void initSimple(){
		timer = new ImpSimpleTimer();
		TimerClientCandidate client = new TimerClientCandidate("CLIENT ");
		Date date = getRandomTime(1);
		client.setDate(date);
		System.out.println(client);
		timer.schedule(date, client);
	}
	private Date getRandomTime(int min){

		int extratime =  (int)(Math.random()*40 + min);
		//System.out.println(extratime);
		Date dj = new  Date();
		dj.setSeconds(dj.getSeconds()+extratime);
		return dj;
		
	}
	private void initClientes(long init,long end) throws InterruptedException{
		
		for(long i = init; i<end;i++){
			TimerClientCandidate client = new TimerClientCandidate("CLIENT "+i);

			Date dj;
			if(init == 0)
				dj =getRandomTime(50);
			else
				dj =getRandomTime(0);
			client.setDate(dj);
			System.out.println(client+ "notfiacar "+dj);
			multitimer.schedule(dj, client);
		}
		procesarlinea(reader.nextLine(),end,2*end-init);
	}
	private void procesarlinea(String l, long init,long end) throws InterruptedException {
		if(l.isEmpty()){
				initClientes(init,end);
				return;
		}
		if(l.indexOf("l")!=-1){
			System.out.println( multitimer.getClients());
			procesarlinea(reader.nextLine(),init,end);
			return;
		}
		if(l.indexOf("n")!=-1){
			System.out.println("El numero de  clientes es: " + multitimer.getClients().size());
			procesarlinea(reader.nextLine(),init,end);
			return;
		}
		int n = Integer.parseInt(l);
		TimerClientCandidate client = new TimerClientCandidate("CLIENT "+n);
		System.out.println("Se abortaron las notificaciones a  "+client);
		multitimer.abortAll(client);
		procesarlinea(reader.nextLine(),init,end);
		
	}
}
