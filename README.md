# Timer
Es un pequenio framework para  temporizadores hecho en java, hay dos tipos de temporizadores  -- Un temporizador se programa para que le informe a un cliente en una fecha y hora determinada  --.
#### Clases de alto nivel(Framework):
Hay dos tipos de temporizadores: simples y multiples, el simple solo puede programar una tarea, encambio el mutiple puede programa.
#### Clases de bajo nivel(Implementaciones):
Hay dos clases que implementan el framework:
- ImpSimpleTimer: Implementacion de un SimpleTimer.
- TimerSRT: Multitimer que utiliza la logica del algorito Shortest remaining Time
#### Uso:
Para implementar cualquiera de las clases de alto nivel hay que implementar la interfaz ITimer, que define un metodo `schedule`  el cual programa un tarea en el timer
